package com.bnymellon.training.api.config;

import java.time.LocalDate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	@Bean
	public Docket documentation() {
		return new Docket(DocumentationType.SWAGGER_2).select()
			.apis(RequestHandlerSelectors.any())
			.paths(PathSelectors.regex("/api/.*"))
			.build()
			.pathMapping("/")
			.directModelSubstitute(LocalDate.class, String.class)
			.genericModelSubstitutes(ResponseEntity.class)
			.useDefaultResponseMessages(false)
			.enableUrlTemplating(false)
			.apiInfo(metadata());
	}

	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("Sample Accounts")
			.description("")
			.version("v1")
			.license("BNY Mellon | Terms Of Use")
			.licenseUrl("https://www.bnymellon.com/us/en/terms-of-use.jsp")
			.termsOfServiceUrl("https://www.bnymellon.com/us/en/terms-of-use.jsp")
			.build();
	}
}
