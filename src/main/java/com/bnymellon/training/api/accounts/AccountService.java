package com.bnymellon.training.api.accounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

@Service
public class AccountService {

    private HashMap<String, Account> accounts;

    public AccountService() {
        accounts = new HashMap<>();
        Account newAccount = new Account("123", 500.0, "Bob", "Smith");
        accounts.put(newAccount.getAccountId(), newAccount);
    }

    public List<Account> getAllAccounts() {
        return new ArrayList<>(accounts.values());
    }

    public void createAccount(Account newAccount) {
        accounts.put(newAccount.getAccountId(), newAccount);
    }

    public Optional<Account> getAccountById(String id) {
        return Optional.ofNullable(accounts.get(id));
    }

    public boolean updateAccount(Account account) {
        return accounts.replace(account.getAccountId(), account) != null;
    }

    public boolean deleteAccount(String id) {
        return accounts.remove(id) != null;
    }

}
