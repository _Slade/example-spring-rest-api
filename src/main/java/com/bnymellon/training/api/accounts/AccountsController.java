package com.bnymellon.training.api.accounts;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class AccountsController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public List<Account> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Account createAccount(@RequestBody Account newAccount) {
        accountService.createAccount(newAccount);
        return newAccount;
    }

    private ResponseEntity accountNotFoundResponse() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
            "<html><head></head><body>"
            + "<h1>Account not found</h1>"
            + "<p>Make sure you have the correct account ID.</p>"
            + "</body></html>"
        );
    }

    @RequestMapping(
        value = "/accounts/{id}",
        method = RequestMethod.GET
    )
    public ResponseEntity getAccountById(@PathVariable String id) {
        Optional<Account> account = accountService.getAccountById(id);
        if (account.isPresent()) {
            return ResponseEntity.ok(account.get());
        }
        return accountNotFoundResponse();
    }

    @RequestMapping(
        value = "/accounts/{id}",
        method = RequestMethod.POST
    )
    public ResponseEntity updateAccount(@RequestBody Account account) {
        return accountService.updateAccount(account)
            ? ResponseEntity.ok(account)
            : accountNotFoundResponse();
    }

    @RequestMapping(
        value = "/accounts/{id}",
        method = RequestMethod.DELETE
    )
    public ResponseEntity deleteAccount(@PathVariable String id) {
        return accountService.deleteAccount(id)
            ? ResponseEntity.ok(null)
            : accountNotFoundResponse();
    }

}
