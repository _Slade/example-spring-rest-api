package com.bnymellon.training.api.accounts;

public class Account {

	private String accountId;
	private double balance;
	private String ownerFirstName;
	private String ownerLastName;

    public Account() { }

    public Account(String accountId, double balance, String ownerFirstName, String ownerLastName) {
		this.accountId = accountId;
		this.balance = balance;
		this.ownerFirstName = ownerFirstName;
		this.ownerLastName = ownerLastName;
    }

	public void setAccountId(String accountId) {
    	this.accountId = accountId;
	}

	public String getAccountId() {
    	return accountId;
	}

	public void setBalance(double balance) {
    	this.balance = balance;
	}

	public double getBalance() {
    	return balance;
	}

	public void setOwnerFirstName(String ownerFirstName) {
    	this.ownerFirstName = ownerFirstName;
	}

	public String getOwnerFirstName() {
    	return ownerFirstName;
	}

	public void setOwnerLastName(String ownerLastName) {
    	this.ownerLastName = ownerLastName;
	}

	public String getOwnerLastName() {
    	return ownerLastName;
	}

}
