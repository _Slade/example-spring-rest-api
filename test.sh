#!/usr/bin/env bash

API="http://localhost:8080/api/v1"

[[ -n $1 ]] || exit

case $1 in
    getAll)
        curl -X GET --header 'Accept: application/json' "${API}/accounts" | jq .
        ;;
    create)
        curl -X POST --header 'Content-Type: application/json' \
            --header 'Accept: application/json' -d '{ "accountId": "555", "balance": 0, "ownerFirstName": "Bob", "ownerLastName": "Jones"  }' \
            "${API}/accounts"
        ;;
    update)
        curl -X POST --header 'Content-Type: application/json' \
            --header 'Accept: application/json' -d '{ "accountId": "555", "balance": 50, "ownerFirstName": "Bob", "ownerLastName": "Jones"  }' \
            "${API}/accounts/555"
        ;;

    delete)
        curl -X DELETE --header 'Accept: */*' 'http://localhost:8080/api/v1/accounts/555'
        ;;

    default)
        ;;

    *) echo "Unknown command: $1" ;;
esac
